# In-memory Eternal-Twin client

In-memory eternal-twin client. This implementation enables to use the application in a development environment without having to configure a database.

## Actions

- `yarn build`: Compile the library
- `yarn test`: Compile the tests and run them
- `yarn lint`: Check for common errors and style issues.
- `yarn format`: Attempt to fix style issues automatically.
